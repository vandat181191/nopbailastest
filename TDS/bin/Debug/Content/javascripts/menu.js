'use strict';
document.addEventListener("DOMContentLoaded",function(){
	const jdiToggle = document.getElementById('ati-toggle');
	const jdiHeader = document.getElementById('ati-header');
	function iconToggle() {
		jdiToggle.classList.toggle('jdi--td');
		jdiToggle.classList.toggle('jdi--ta');
	}
	function navToggle() {
		jdiHeader.classList.toggle('jdi--hd');
		jdiHeader.classList.toggle('jdi--ha');
	}
	document.querySelector('#ati-toggle').addEventListener('click', iconToggle);
	document.querySelector('#ati-toggle').addEventListener('click', navToggle);
});
