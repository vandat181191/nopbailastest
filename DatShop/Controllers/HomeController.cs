﻿using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DatShop.Controllers
{
    public class HomeController : Controller
    {
        
        public ActionResult Header()
        {
            List<Item> lstItem = new List<Item>();
            Item item = Sitecore.Context.Database.GetItem(RenderingContext.Current.Rendering.DataSource);
            if(item!=null)
            {
                lstItem = item.GetChildren().ToList();
            }
            
            return View("~/Views/Layouts/Header/Header.cshtml",lstItem);
        }
        public ActionResult ContentPage()
        {
            List<Item> lstItem = new List<Item>();
            Item item = Sitecore.Context.Database.GetItem(RenderingContext.Current.Rendering.DataSource);
            if(item!=null)
            {
                lstItem = item.GetChildren().ToList();
            }
             
            return View("~/Views/Layouts/Body/ContentPage.cshtml", lstItem);
        }
        public ActionResult Footer()
        {
            List<Item> lstItem = new List<Item>();
            Item item = Sitecore.Context.Database.GetItem(RenderingContext.Current.Rendering.DataSource);
            if(item!=null)
            {
                lstItem = item.GetChildren().ToList();
            }
                 
            return View("~/Views/Layouts/Footer/Footer.cshtml", lstItem);
        }
        public ActionResult Detail()
        {
            Item item = Sitecore.Context.Database.GetItem(RenderingContext.Current.Rendering.DataSource);
            return View("~/Views/Layouts/Body/Detail.cshtml",item);
        }
        public ActionResult DetailGlossary()
        {
                Item item = Sitecore.Context.Database.GetItem(RenderingContext.Current.Rendering.DataSource);
                return View("~/Views/Layouts/Body/DetailGlossary.cshtml", item);
        }
        public ActionResult ListGlossary()
        {
                
                List<Item> lstItem = new List<Item>();
                Item item = Sitecore.Context.Database.GetItem(RenderingContext.Current.Rendering.DataSource);
                if(item!=null)
                {
                    lstItem = item.GetChildren().ToList();
                }
                
                return View("~/Views/Layouts/Body/ListGlossary.cshtml", lstItem);
           
           
        }
    }
}